# paddington
Please look after this bear. Thank you. 🐻

## 使い方
1. Git, Node.js (v12以降) をインストール
2. `git clone https://github.com/tuxsnct/paddington.git`を実行する
3. `yarn install`を実行する
4. `.env`ファイルの`PADDINGTOM_ID`と`PADDINGTOM_PASSWORD`にIDとパスワードを記入する
5. `yarn start`を実行する

# ライセンス
Apache-2.0
